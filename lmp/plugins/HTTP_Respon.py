#!/usr/bin/python
from __future__ import print_function
from bcc import BPF
from bcc.utils import printb
from socket import ntohs,inet_ntop,AF_INET
from time import sleep
from struct import pack
dns_bpf_text = """
#include <net/inet_sock.h>
#include <net/tcp.h>
#include <linux/types.h>
BPF_PERF_OUTPUT(http_events);
BPF_HASH(currsock, struct sock *, struct task_struct *);
struct http{
    u32 pid;
    u32 tgid;
    u32 saddr;
    u32 daddr;
    u32 dport;
    u32 sport;
    char common[TASK_COMM_LEN];
};

int trace_tcp_sendmsg(struct pt_regs *ctx, struct sock *sk)
{
    struct task_struct *t = (struct task_struct *)bpf_get_current_task();
    currsock.update(&sk,&t);
    return 0;
}

int trace_tcp_v4_rcv(struct pt_regs *ctx)
{
    struct task_struct **t;
    struct sk_buff *skb = (struct sk_buff *)PT_REGS_PARM1(ctx);
    if(skb == 0)
     return 0;
    struct sock *sk = skb->sk;
    if(sk==0)
        return 0;
    t = currsock.lookup(&sk);
    if (t == 0) {
        return 0;   
    }
    struct task_struct *task=*t;
    u32 pid = task->pid;
    u32 tgid = task->tgid;
    struct task_struct *leader=task->group_leader;

    const struct tcphdr *th = (const struct tcphdr *)skb->data;
    if(th == 0)
        return 0;
    if (th->source == ntohs(80) || th->source == ntohs(443)) {
        struct http tp = {};
        bpf_probe_read_kernel(&tp.common,TASK_COMM_LEN,&(leader->comm));

        tp.pid = pid;
        tp.tgid = tgid;
        tp.dport = th->source;
        tp.sport = th->dest;
        tp.saddr = sk->__sk_common.skc_rcv_saddr;
        tp.daddr = sk->__sk_common.skc_daddr;
        http_events.perf_submit(ctx, &tp, sizeof(tp));
    }
    return 0;
}
"""

# process event
def respon_http(cpu, data, size):
    event = b["http_events"].event(data)

    printb(b"%-6d %-6d %-12.12s  %-16s %-6d %-16s %-6d " % (event.tgid,event.pid,event.common,inet_ntop(AF_INET, pack("I", event.saddr)).encode(), ntohs(event.sport),inet_ntop(AF_INET, pack("I", event.daddr)).encode(), ntohs(event.dport)))
b = BPF(text=dns_bpf_text)
b.attach_kprobe(event="tcp_v4_rcv", fn_name="trace_tcp_v4_rcv")
b.attach_kprobe(event="tcp_sendmsg", fn_name="trace_tcp_sendmsg")

b["http_events"].open_perf_buffer(respon_http)
while True:
    try:
        b.perf_buffer_poll()
    except KeyboardInterrupt:
        exit()
