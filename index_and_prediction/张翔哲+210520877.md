# 项目信息

## 项目信息

- 项目名称：HTTP、DNS网络态势预测

本题目要求使用eBPF技术实现HTTP、DNS的性能监控，不限于传统的网络指标，需要探索Linux内核中可以利用的HTTP、DNS数据，要求至少从以下6个处理维度中选取4个维度对HTTP、DNS性能进行刻画展示：1、HTTP请求；2、HTTP响应；3、HTTP延时；4、DNS请求5、DNS响应6、DNS延时，从而达到探索Linux内核的目的。

- 方案描述：使用`uprobe`提取DNS延时、使用`kprobe`从`TCP/UDP`层分别提取`DNS`请求响应、`HTTP`响应数据

**项目整体架构:**

如图 1 所示，项目整体架构分为数据提取模块和预测模块。

![structer](https://raw.githubusercontent.com/JiaoTuan/Images-JiaoTuan/master/summer_6000/structer.6gbnaje02uc0.png)

<center>图1 整体架构图</center>

通过eBPF技术将Linux系统中用户网络流量数据提取出后通过LSTM预测模块进行预测。

* 时间规划：

<center>表1 开源之夏时间规划表</center>

|     日期      |            事项             |
| :-----------: | :-------------------------: |
| 07/01 - 07/7  |     研究eBPF原理与使用      |
| 07/8 - 07/14  | 用户态使用uprobe提取DNS延时 |
| 07/15 - 07/21 |    学习UDP层数据发送流程    |
| 07/22 - 07/28 |       提取DNS请求信息       |
| 07/29 - 08/04 |    学习UDP层数据接收过程    |
| 08/05 - 08/11 |       提取DNS响应信息       |
| 08/12 - 08/15 |      准备中期检查文档       |
| 08/16 - 08/23 |    学习TCP层数据收发流程    |
| 08/23 - 08/30 |      提取HTTP数据指标       |
| 09/01 - 09/15 |        预测算法学习         |
| 09/16 - 09/23 |          预测实验           |
| 09/24 - 09/30 |        完成项目收尾         |

# 数据提取模块

该模块主要提取`HTTP`、`DNS`指标数据信息，并将信息存放在`excel`文件中。

鉴于应用层发送接收数据函数比较多样，本文两种指标请求和响应信息均在传输层进行提取，使用`kprobe`作为程序挂载点。延时指标在应用层提取

## HTTP响应&请求

`http`底层主要基于`TCP`协议实现，`TCP`协议内核函数实现如图 2 所示。

![TCP](https://raw.githubusercontent.com/JiaoTuan/Images-JiaoTuan/master/summer_6000/TCP.233xp4ruhgao.png)

<center>图2 TCP协议内核实现</center>

### 1. TCP挂载函数分析

选择`tcp_sendmsg`与`tcp_v4_rcv为tcp`数据发送与接收挂载函数。

**tcp_sendmsg:**应用层产生的数据包需要复制到内核接受缓冲区由tcp_sendmsg函数完成

```c
int tcp_sendmsg(struct sock *sk, struct msghdr *msg, size_t size)
```

**tcp_recvmsg:**将数据从接受队列中复制到用户空间

```c
int tcp_v4_rcv(struct sk_buff *skb)
```

**struct sock结构体：**

`sock`结构是维系`BSD socket`层和`INET socket`层的纽带。

从该结构体中我们可以获得源端口、目的端口、源IP地址、目的IP地址等。

> include/net/sock.h

```c
struct sock {
	/*
	 * Now struct inet_timewait_sock also uses sock_common, so please just
	 * don't add nothing before this first member (__sk_common) --acme
	 */
	struct sock_common	__sk_common;
#define sk_node			__sk_common.skc_node
#define sk_nulls_node		__sk_common.skc_nulls_node
#define sk_refcnt		__sk_common.skc_refcnt
#define sk_tx_queue_mapping	__sk_common.skc_tx_queue_mapping

#define sk_dontcopy_begin	__sk_common.skc_dontcopy_begin
#define sk_dontcopy_end		__sk_common.skc_dontcopy_end
#define sk_hash			__sk_common.skc_hash
#define sk_portpair		__sk_common.skc_portpair
#define sk_num			__sk_common.skc_num
#define sk_dport		__sk_common.skc_dport
#define sk_addrpair		__sk_common.skc_addrpair
#define sk_daddr		__sk_common.skc_daddr
#define sk_rcv_saddr		__sk_common.skc_rcv_saddr
#define sk_family		__sk_common.skc_family
#define sk_state		__sk_common.skc_state
#define sk_reuse		__sk_common.skc_reuse
#define sk_reuseport		__sk_common.skc_reuseport
#define sk_ipv6only		__sk_common.skc_ipv6only
#define sk_net_refcnt		__sk_common.skc_net_refcnt
#define sk_bound_dev_if		__sk_common.skc_bound_dev_if
#define sk_bind_node		__sk_common.skc_bind_node
#define sk_prot			__sk_common.skc_prot
#define sk_net			__sk_common.skc_net
#define sk_v6_daddr		__sk_common.skc_v6_daddr
#define sk_v6_rcv_saddr	__sk_common.skc_v6_rcv_saddr
#define sk_cookie		__sk_common.skc_cookie
#define sk_incoming_cpu		__sk_common.skc_incoming_cpu
#define sk_flags		__sk_common.skc_flags
#define sk_rxhash		__sk_common.skc_rxhash

	socket_lock_t		sk_lock;
	atomic_t		sk_drops;
	int			sk_rcvlowat;
	struct sk_buff_head	sk_error_queue;
	struct sk_buff_head	sk_receive_queue;
  ......
  ......
};
```

**struct sk_buff：**

内核用于管理网络数据包的缓冲区，描述已接收或待发送的数据报文信息。

缓冲区被分配为两部分：

1. sk_buff类型的固定大小的标头,SKB描述符
2. 足以容纳单个数据包的全部或部分数据的可变长度区域

**源码：**

> 2.6版本

> /include/linux/skbuff.h


```c
struct sk_buff {
	/* These two members must be first. */
	struct sk_buff		*next;
	struct sk_buff		*prev;

	struct sk_buff_head	*list;
	struct sock		*sk;              //指向拥有此缓冲区的套接字sock结构
	struct timeval		stamp;         
	struct net_device	*dev;          
	struct net_device	*real_dev;     
	
	union {
		struct tcphdr	*th;
		struct udphdr	*uh;
		struct icmphdr	*icmph;
		struct igmphdr	*igmph;
		struct iphdr	*ipiph;
		unsigned char	*raw;
	} h;

	union {
		struct iphdr	*iph;
		struct ipv6hdr	*ipv6h;
		struct arphdr	*arph;
		unsigned char	*raw;
	} nh;

	union {
	  	struct ethhdr	*ethernet;
	  	unsigned char 	*raw;
	} mac;

	struct  dst_entry	*dst;
	struct	sec_path	*sp;

	/*
	 * This is the control buffer. It is free to use for every
	 * layer. Please put your private variables there. If you
	 * want to keep them across layers you have to do a skb_clone()
	 * first. This is owned by whoever has the skb queued ATM.
	 */
	char			cb[48];//可用于协议之间传递信息

	unsigned int		len,        
				data_len,           
				csum;               //csum字段保留数据的最终校验和
    
	unsigned char		local_df,
                    	/*local_df字段用于在是否请求真实路径mtu时发出信号。
                        local_df==1表示未请求IP_PMTUDISC_DO，
                        local_df==0表示IP_PMTUDISC_DO被请求，因此如果我们收到片段，应该生成icmp错误。
                        */
	
				cloned,//skbuff是否已被克隆
	
				pkt_type, 
                /*描述了包的目的地
                /* Packet types */
                #define PACKET_HOST		0		/*发给接收该数据包的主机(数据包MAC==接收设备MAC)*/
                #define PACKET_BROADCAST	1		/*数据包MAC目的地址是一个广播地址 */
                #define PACKET_MULTICAST	2		/*目的地址是组播地址*/
                #define PACKET_OTHERHOST	3		/* 目的地址与接收设备MAC地址完全不同，若本机没有转发功能，包将被丢弃*/
                #define PACKET_OUTGOING		4		/*这个数据包将被发出*/
                /* These ones are invisible by user level */
                #define PACKET_LOOPBACK		5		/*数据包被发给lookup设备*/
                #define PACKET_FASTROUTE	6		/*这个数据包由快速路由代码查找路由*/
				ip_summed;
                /*告诉驱动程序是否提供校验和
                /* Don't change this without changing skb_csum_unnecessary! */
                #define CHECKSUM_NONE 0         //输入时，表示设备无法校验数据包，由软件校验
                #define CHECKSUM_UNNECESSARY 1  //没有必要校验
                #define CHECKSUM_COMPLETE 2     //已经完成执行校验和
                #define CHECKSUM_PARTIAL 3      //由硬件执行校验和
                */
	__u32			priority;
	unsigned short		protocol,
				security;//安全级别

	void			(*destructor)(struct sk_buff *skb);     
	
	
#ifdef CONFIG_NETFILTER
        unsigned long		nfmark;
	__u32			nfcache;
	struct nf_ct_info	*nfct;
#ifdef CONFIG_NETFILTER_DEBUG
        unsigned int		nf_debug;
#endif
#ifdef CONFIG_BRIDGE_NETFILTER
	struct nf_bridge_info	*nf_bridge;
#endif
#endif /* CONFIG_NETFILTER */
#if defined(CONFIG_HIPPI)
	union {
		__u32		ifield;
	} private;
#endif
#ifdef CONFIG_NET_SCHED
       __u32			tc_index;               /* traffic control index */
#endif

	/* These elements must be at the end, see alloc_skb() for details.  */
	unsigned int		truesize;   
	atomic_t		users;     
	unsigned char		*head,
				*data,
				*tail,
				*end;
};
```

==内核维护sk_buff总布局==

内核在一个双向链表中维护所有sk_buff结构，如图 3 所示。

> /include/linux/skbuff.h

每个SKB必须能被整个链表头快速找到，所以有了sk_buff_head

```c
struct sk_buff_head {
	/* These two members must be first. */
	struct sk_buff	*next;
	struct sk_buff	*prev;

	__u32		qlen;       //表中元素数目
	spinlock_t	lock;       //防止并发访问
};
```

```c
struct sk_buff {
    ......
	struct sk_buff		*next;
	struct sk_buff		*prev;

	struct sk_buff_head	*list;
    ......
};
```

![image](https://raw.githubusercontent.com/JiaoTuan/Images-JiaoTuan/master/kernel_net/sh_buff元素列表.39cik8geoxk0.png)

<center>图3 sk_buff链表结构</center>

==协议的头指针==

针对传输层(L4)

```c

	union {
		struct tcphdr	*th;
		struct udphdr	*uh;
		struct icmphdr	*icmph;
		struct igmphdr	*igmph;
		struct iphdr	*ipiph;
		unsigned char	*raw;   //用于初始化
	} h;
```
针对网络层(L3)
```c
	union {
		struct iphdr	*iph;
		struct ipv6hdr	*ipv6h;
		struct arphdr	*arph;
		unsigned char	*raw;   //用于初始化
	} nh;
```

**IP头部:**

```c
struct iphdr {
#if defined(__LITTLE_ENDIAN_BITFIELD)
	__u8	ihl:4,
		version:4;
#elif defined (__BIG_ENDIAN_BITFIELD)
	__u8	version:4,
  		ihl:4;
#else
#error	"Please fix <asm/byteorder.h>"
#endif
	__u8	tos;
	__u16	tot_len;
	__u16	id;
	__u16	frag_off;
	__u8	ttl;
	__u8	protocol;
	__u16	check;
	__u32	saddr;
	__u32	daddr;
	/*The options start here. */
};
```

针对数据链路层(L2)
```c
	union {
	  	struct ethhdr	*ethernet;
	  	unsigned char 	*raw;   //用于初始化
	} mac;
```

**MAC头定义:**

> /include/linux/if_ether.h

```c
struct ethhdr 
{
	unsigned char	h_dest[ETH_ALEN];	/* destination eth addr	*/
	unsigned char	h_source[ETH_ALEN];	/* source ether addr	*/
	unsigned short	h_proto;		/* packet type ID field	*/
};
```

==路由相关字段==

目的路由缓存项。输入输出数据包都要经过路由子系统查询得到目的路由缓存项，确定数据包流向，否则只能丢弃。
```c
struct  dst_entry *dst; //路由子系统使用
```
指向用于路由sk_buff的路由缓存条目的指针。它所指向的这个路由缓存元素包含指向该函数的指针将被调用以转发该数据包。此指针dst必须指向有效的路由缓存元素，然后才能将缓冲区传递到IP层进行传输

```c
struct	sec_path	*sp;
```
sec_path指针是一个相对较新的可选字段，它支持网络安全的附加“钩子”。

==长度字段==

缓冲区中数据区块的大小：主缓冲区数据+片段缓冲区数据
```c
unsigned int		len,
```
片段中的数据大小,表示留在片段列表和未映射页面缓冲区中的字节总和。用来判断是否开启聚合分散IO数据
```c
				data_len;
```
每当len增加该字段都会更新，由alloc_skb函数设置整个缓冲区的总长度(SKB+数据缓存区)
```c
unsigned int		truesize;		
```

==引用计数==

标识有多少用户引用了该SKB，确定释放SKB的时机

```c
atomic_t		users;
```

* 每当共享缓冲区时，它就会递增。

* 当逻辑上释放缓冲区时，它会递减。

* 仅当引用计数达到0时，缓冲区才会被物理上释放。

==网络设备字段==

时间戳只对一个已接收的封包有意义，表示封包何时被接接收或封包的预定传输时间。

配套函数：netif_rx()，net_timestamp()

```c
struct timeval		stamp;         
```
网络设备指针。与SKB是发送包还是接收包有关。

初始化网络设备驱动，分配接收缓存队列时，该指针指向收到数据包的网络设备。

```c
struct net_device	*dev;
```

该字段只对虚拟设备有意义，代表与虚拟设备关联的真实设备。

```c
struct net_device	*real_dev; 
```

接收报文的原始网络设备。若包是本地生成，设置为NULL

```c
struct net_device	*input_dev;
```

==缓冲区内容管理指针==

其结构如图 4 所示

```c
unsigned int	len,    

......

unsigned char	*head,
				*data,
				*tail,
				*end;
```

**head:** 指向缓冲区第一个字节，缓冲区创建是指向，之后永远不会改变

**data:** 指向缓冲区中“数据”的开始位置。当头数据被移除或添加到数据包时，可以向前或向后调整此指针。

**tail:** 指向缓冲区中“数据”最后的字节。该指针也可以被调整。

**end:** 指向skb_shared_info结构的开始(即。数据包数据存储区域之外的第一个字节。)它也被设置在缓冲区分配时间，此后从不调整。

**len:** tail-data。

![buffer_mangment](https://raw.githubusercontent.com/JiaoTuan/Images-JiaoTuan/master/kernel_net/buffer_mangment.6siay468vq00.png)

<center>图4 缓冲区指针结构</center>

==skb_shared_info结构==

skb_shared_info用于管理碎片化的缓冲区和未映射的页面缓冲区，其结构如图 5 所示。

这个结构位于数据区域的末端，由sk_buff的end指针指向。
```c
/* This data is invariant across clones and lives at
 * the end of the header data, ie. at skb->end.
 */
struct skb_shared_info {
	atomic_t	dataref;
	unsigned short	nr_frags;
	unsigned short	gso_size;
	/* Warning: this field is not always filled in (UFO)! */
	unsigned short	gso_segs;
	unsigned short  gso_type;
	__be32          ip6_frag_id;
	struct sk_buff	*frag_list;
	skb_frag_t	frags[MAX_SKB_FRAGS];
};
```

当单个sk_buff的数据由多个片段组成时，将使用`skb_shared_info`结构。

**dataref:** 当一个数据缓冲区被多个SKB描述符引用时，设置该字段。

nr_frags、frags、frag_list与IP分片存储有关。用于支持聚合分散I/O数据

**聚合分散 I/O数据：** 网络中创建一个发送报文的过程包括组合多个片。报文数据必须从用户空间复制到内核空间，同时加上网络协议栈各层的首部，这个组合可能要求相当数量的数据拷贝。但如果发送报文的网络接口能够支持聚合分散I/O，报文就无需组装成一个单块，可避免大量拷贝。聚合分散 I/O 从用户空间启动"零拷贝"网络发送。内核传递发送报文给 `hard_start_xmit（）`之前需要判断网络设置是否支持 `NETIF_ F_SG`，不然只能对分散的报文进行线性化处理，再聚合成一个单独的报文。如果网络设备已经设置了该标志，接下来就要检查 nr_frags 的值，因为该字段确定了片段数，这些分散的片段以关联的方式存储在 frags数组中。

`frags`和`nr_frags`用于支持SG类型的聚合分散I/O分片。frags是用于存放聚合分散 I/O分片的数组。

`skb_is_nonlinear()`函数通过判断`data_len`可以知道SKB是否存在聚合分散I/O 缓存区。

***frag_list：** 

1. 用于在接收分片组后链接多个分片，组成一个完整的 IP 数据报。
2. 在 UDP 数据报的输出中，将待分片的 SKB 链接到第一个 SKB中，然后在输出过程中能够快速地分片。
3. 用于存放 `FRAGLIST`类型的聚合分散 I/O 的数据包，如果输出网络设备支持 FRAGLIST类型的聚合分散 I/O，则可以直接输出。

**nr_frags：** 当前使用`聚合分散 I/O `分片的数量,即为 `frags` 数组中使用的数量，不超过 `MAX_SKB_FRAGS`

**frags[]：** frags数组保留指向碎片化`subuff`的页面结构的指针。最后使用的页面指针是nr_frags-1，有最多为`MAX_SKB_FRAGS`的空间。

skb_frag_t结构表示一个未映射的页面缓冲区。

```c
/* To allow 64K frame to be packed as single skb without frag_list */
#define MAX_SKB_FRAGS (65536/PAGE_SIZE + 2)

typedef struct skb_frag_struct skb_frag_t;

struct skb_frag_struct {
	struct page *page;
	__u32 page_offset;
	__u32 size;
};
```

**page：** 指向文件系统缓存页面的指针。

**page_offset:** 存储数据的页面偏移。

**size:** 数据的长度

![skb_shared_info](https://raw.githubusercontent.com/JiaoTuan/Images-JiaoTuan/master/kernel_net/skb_shared_info.6nxrqw6dyqc0.png)

<center>图5 sk_share_info</center>


data_len = 0,说明没有报文聚合分散IO数据。nr_frags = 0，frag_list = NULL说明未开启分片

![存储在线性存储区的报文](https://raw.githubusercontent.com/JiaoTuan/Images-JiaoTuan/master/kernel_net/存储在线性存储区的报文.4ou75ut7gzy0.png)

<center>图6 存储在线性存储区的报文</center>



![skb_shared_info_list](https://raw.githubusercontent.com/JiaoTuan/Images-JiaoTuan/master/kernel_net/skb_shared_info_list.hsivj5hit74.png)

<center>图7 skb_shared_info_list</center>

### 2. 用户态python代码分析

使用kprobe挂载函数挂载`tcp_sendmsg`与`tcp_recvmsg`。

```python
b = BPF(text=dns_bpf_text)
b.attach_kprobe(event="tcp_v4_rcv", fn_name="trace_tcp_v4_rcv")
b.attach_kprobe(event="tcp_sendmsg", fn_name="trace_tcp_sendmsg")
```

从BPF Map中获取数据并输出：

```python
# process event
def respon_http(cpu, data, size):
    event = b["http_events"].event(data)

    printb(b"%-6d %-6d %-12.12s  %-16s %-6d %-16s %-6d " % (event.tgid,event.pid,event.common,inet_ntop(AF_INET, pack("I", event.saddr)).encode(), ntohs(event.sport),inet_ntop(AF_INET, pack("I", event.daddr)).encode(), ntohs(event.dport)))

b["http_events"].open_perf_buffer(respon_http)
while True:
    try:
        b.perf_buffer_poll()
    except KeyboardInterrupt:
        exit()
```

### 3. 内核kern核心代码分析

80与433端口过滤确定是`http`或`https`数据：

```c
th->source == ntohs(80) || th->source == ntohs(443)
```

在数据报发送时通过挂载`tcp_sendmsg`触发函数`trace_tcp_sendmsg`获得发送数据包`task_struct`并存入BPF Map中。

```c
int trace_tcp_sendmsg(struct pt_regs *ctx, struct sock *sk)
{
    struct task_struct *t = (struct task_struct *)bpf_get_current_task();
    currsock.update(&sk,&t);
    return 0;
}
```

在tcp接收函数挂载函数中以sock为key查询该task_struct，确保只提取这同一进程的信息，过滤不必要数据包影响。并从中提取相关信息。

```c
int trace_tcp_v4_rcv(struct pt_regs *ctx)
{ 
  ......
  struct sock *sk = skb->sk;
    
  t = currsock.lookup(&sk);
      if (t == 0) {
          return 0;   
      }
  u32 pid = task->pid;
  u32 tgid = task->tgid;
  
  tp.pid = pid;
  tp.tgid = tgid;
  tp.dport = th->source;
  tp.sport = th->dest;
  tp.saddr = sk->__sk_common.skc_rcv_saddr;
  tp.daddr = sk->__sk_common.skc_daddr;
  ......
}
```

从`sk_buff`中获得`tcphdr`结构，并从中提取数据。

```c
int trace_tcp_v4_rcv(struct pt_regs *ctx)
{ 
  ......
  struct sk_buff *skb = (struct sk_buff *)PT_REGS_PARM1(ctx);
  const struct tcphdr *th = (const struct tcphdr *)skb->data;
  ......
  tp.dport = th->source;
  tp.sport = th->dest;
}
```

**struct tcphdr: ** 

```c
struct tcphdr {
	__be16	source;//16位源端口号
	__be16	dest;//16位目的端口号
	__be32	seq;//此次发送的数据在整个报文段中的起始字节数。
	__be32	ack_seq//是下一个期望接收的字节，确认序号应当是上次已成功接收的序号+1，只有ack标志为1时确认序号字段才有效。
#if defined(__LITTLE_ENDIAN_BITFIELD)
	__u16	res1:4,// 保留位
		doff:4,//tcp头部长度，指明了在tcp头部中包含了多少个32位的字
		fin:1,//发端完成发送任务
		syn:1,//同步序号用来发起一个连接
		rst:1,//重建连接
		psh:1,//接收方应该尽快将这个报文段交给应用层
		ack:1,//一旦一个连接已经建立了，ack总是=1
		urg:1,//紧急指针有效
		ece:1,
		cwr:1;
#elif defined(__BIG_ENDIAN_BITFIELD)
	__u16	doff:4,
		res1:4,
		cwr:1,
		ece:1,
		urg:1,
		ack:1,
		psh:1,
		rst:1,
		syn:1,
		fin:1;
#else
#error	"Adjust your <asm/byteorder.h> defines"
#endif	
	__be16	window;//窗口大小，单位字节数，指接收端正期望接受的字节，16bit，故窗口大小最大为16bit=65535字节
	__sum16	check;//校验和校验的是整个tcp报文段，包括tcp首部和tcp数据，这是一个强制性的字段，一定是由发端计算和存储，
	__be16	urg_ptr;
};
```

将数据放入`BPF Map`中

```c
bpf_probe_read_kernel(&tp.common,TASK_COMM_LEN,&(leader->comm));
```

### 4. 运行结果

![HTTP_request](https://raw.githubusercontent.com/JiaoTuan/Images-JiaoTuan/master/kernel_net/HTTP_request.4v93hwbiw8o0.png)

<center>图8 HTTP响应运行结果</center>

## DNS指标

### 1. DNS请求

#### 1.1 UDP请求挂载函数分析

应用程序调用`send()`等一系列系统调用向UDP写数据时，经过多次函数调用如下所示，最终会调用到传输层`udp_sendmsg()`函数。

```c
sys_send()
  	--->sockfd_lookup_light()
  	--->sock_sendmsg()
  			--->__sock_sendmsg()
  					--->sock->ops->sendmsg()---proto_ops--->inet_sendmsg()
  					--->sk->sk_prot->sendmsg()---proto--->udp_sendmsg()
```

**udp_sendmsg()函数定义：**

> net/ipv4/udp.c

```c
int udp_sendmsg(struct sock *sk, struct msghdr *msg, size_t len)
```

**参数二struct msghdr：**

`udp_sendmsg`函数的第二个参数类型，主要用于向一个socket发送消息，或从一个socket中接收消息，此处用来描述发送数据的`msghdr`指针。

```c
struct msghdr {
	void            *msg_name;      /* [XSI] optional address */
	socklen_t       msg_namelen;    /* [XSI] size of address */
	struct          iovec *msg_iov; /* [XSI] scatter/gather array */
	int             msg_iovlen;     /* [XSI] # elements in msg_iov */
	void            *msg_control;   /* [XSI] ancillary data, see below */
	socklen_t       msg_controllen; /* [XSI] ancillary data buffer len */
	int             msg_flags;      /* [XSI] flags on received message */
};
```

其中`msg_iov`记录了数据区首地址以及数据长度，其结构如下所示

**struct  iovec:**

```c
struct iovec {
	void *   iov_base;      /* [XSI] Base address of I/O memory region */
	size_t   iov_len;       /* [XSI] Size of region iov_base points to */
};
```

其中`iov_base`指向数据区地址，`iov_len`为数据区长度

#### 1.2 用户态python代码分析

使用BCC提供挂载函数挂载`udp_sendmsg`函数并将回掉函数设置为`trace_udp_sendmsg`

```python
b = BPF(text=dns_bpf_text)
b.attach_kprobe(event="udp_sendmsg", fn_name="trace_udp_sendmsg")
```

`udp_sendmsg`被触发后，数据被内核函数放入Map中，这时用户态使用python代码将数据从BPFMap中取出，并通过dnslib库对数据进行解析进行输出，DNS请求报文结构如图 9 所示。

![DNS_requestion](https://raw.githubusercontent.com/JiaoTuan/Images-JiaoTuan/master/DNS/DNS_requestion.6ue1pepq5as0.png)

<center>图9 DNS请求数据报结构</center>

==QNAM：== 要查找的名字，一个或多个标识符的序列，其中每个标识符由一个长度的八bit字节和该数量的八位字节组成。每个名字以最后字节为0结束，长度为0的标识符是根标识符。单个标识符最大长度为63字节。

==QTYPE：== 查询类型，每个问题都有一个查询类型，2字节。

**A：** 1 期望获得查询名IP地址

A 记录只保存 IPv4 地址。如果网站有IPv6地址，它将改为使用“AAAA”记录。


**NS：** 2 存储DNS条目的域名服务器

NS 代表“域名服务器”，域名服务器记录指示哪个DNS服务器对该域具有权威性

**CNAME：** 5 规范名称，将一个域或子域转发到另一个域，不提供 IP 地址。

当域或子域是另一个域的别名时，使用“规范名称”(CNAME) 记录代替A 记录。所有 CNAME 记录都必须指向域，而不是IP 地址。

**PTR：** 12 指针记录，在反向查询中提供域名

当用户尝试在其浏览器中访问域名时，会进行 DNS 查找，将域名与 IP 地址进行匹配。反向 DNS 查找与此过程相反：它是一个以 IP 地址开头并查找域名的查询。

HINFO 13 主机信息

MX 15 邮件交换记录

AXFR 252 对区域转换的请求

ANY 255 对所有记录的请求

==QCLASS：== 指定查询类别的两个八位字节代码。

由于比较简单，python中直接输出不做字段解析。

```python
def print_ipv4_event(cpu, data, size):
     event = b["dns_events"].event(data)
     payload = event.pkt[:size]
     dnspkt = dnslib.DNSRecord.parse(payload)
     print(dnspkt)

b["dns_events"].open_perf_buffer(print_ipv4_event)

while True:
        try:
            b.perf_buffer_poll()
        except KeyboardInterrupt:
            exit()
```

#### **1.3 内核kern核心代码分析**

```python
dns_bpf_text = """
#include <net/inet_sock.h>

struct dns_data_t {
   u8  pkt[200];
};

BPF_PERF_OUTPUT(dns_events);

int trace_udp_sendmsg(struct pt_regs *ctx)
{
        struct dns_data_t data ={};
        struct msghdr *msghdr = (struct msghdr *)PT_REGS_PARM2(ctx);
        struct sock *sk = (struct sock *)PT_REGS_PARM1(ctx);
        struct inet_sock *is = inet_sk(sk);
        
        if (msghdr == 0)
            return 0;
        if (is->inet_dport == 13568) {
            void *iovbase = msghdr->msg_iter.iov->iov_base;
            if(iovbase == 0)
             return 0;
            bpf_probe_read(data.pkt,200, iovbase);
            dns_events.perf_submit(ctx, &data, sizeof(data));
        }
        return 0;
}
"""
```

其中通过端口过滤保证所处理数据为53端口DNS数据，端口信息数据来源于sock结构，该结构为挂载第一个参数通过`PT_REGS_PARM1(ctx)`获取：

```c
struct sock *sk = (struct sock *)PT_REGS_PARM1(ctx);
is->inet_dport == 13568
```

DNS请求eBPF提取程序就从该结构体提取数据，其核心代码如下：

```c
void *iovbase = msghdr->msg_iter.iov->iov_base;
if(iovbase == 0)
		return 0;
bpf_probe_read(data.pkt,200, iovbase);
```

通过BPF帮助函数将得到数据放入BPF Map中：

```c
 dns_events.perf_submit(ctx, &data, sizeof(data));
```

#### **1.4 运行结果：**

![DNS_request](https://raw.githubusercontent.com/JiaoTuan/Images-JiaoTuan/master/kernel_net/DNS_request.jzl9yhg7bn4.png)

<center>图10 DNS请求运行结果</center>

### 2. DNS响应

#### 2.1 UDP响应挂载函数分析

读操作系统调用到了传输层协议，都调用到了同一接口，对于UDP为`udp_recvmsg`

```c
sys_recvmsg()
  	--->sockfd_lookup_light()
  	--->sock_recvmsg()
  			--->__sock_recvmsg()
  					--->sock->ops->recvmsg()---proto_ops--->inet_recvmsg()
  					--->sk->sk_prot->recvmsg()---proto--->udp_recvmsg()
```

**udp_recvmsg()函数定义：**

> net/ipv4/udp.c

```c
int udp_recvmsg(struct sock *sk, struct msghdr *msg, size_t len, int noblock,
		int flags, int *addr_len)
```

参数与`udp_sendmsg`相，此处不再分析。

#### 2.2 用户态Python代码分析

使用`kprobe`与`kretprobe`挂载`udp_recvmsg`函数。

```python
b = BPF(text=dns_bpf_text)
b.attach_kprobe(event="udp_recvmsg", fn_name="trace_udp_recvmsg")
b.attach_kretprobe(event="udp_recvmsg", fn_name="trace_udp_ret_recvmsg")
```

`udp_recvmsg`被触发后，数据被内核函数放入Map中，这时用户态使用python代码将数据从BPFMap中取出，并通过dnslib库对数据进行解析进行输出，DNS响应报文结构如图 11 所示。

![DNS_respons](https://raw.githubusercontent.com/JiaoTuan/Images-JiaoTuan/master/DNS/DNS_respons.30a7ex1mmg80.png)

<center>图11 DNS响应数据包格式</center>

==NAME：== 被查询的域名，格式与问题中的QNAME相同。

==TYPE：== 包含第一个类型代码的两个八位字节。该字段指定 RDATA 字段中数据的含义。包含第一个类型代码的两个八位字节。该字段指定 RDATA 字段中数据的含义。

==CLASS:== 指定 RDATA 字段中数据类别的两个八位字节。

==TTL：== 结果可以缓存的秒数。

==RDLENGTH：== RDATA 字段的长度。

==RDATA：== 响应的数据。不定长字符串来表示记录，格式根TYPE和CLASS有关。比如，TYPE是A，CLASS 是 IN，那么RDATA就是一个4个字节的ARPA网络地址。

```python
def save_dns(cpu, data, size):
    event = b["dns_events"].event(data)
    payload = event.pkt[:size]

    dnspkt = dnslib.DNSRecord.parse(payload)

    if dnspkt.header.qr != 1:
        return
    if dnspkt.header.q != 1:
        return
    if dnspkt.header.a == 0 and dnspkt.header.aa == 0:
        return
    question = ("%s" % dnspkt.q.qname)[:-1].encode('utf-8')
    for answer in dnspkt.rr:
        # skip all but A and AAAA records
        if answer.rtype == 1 or answer.rtype == 28:
            sleep(1)
            print(str(answer.rdata).encode('utf-8'),question,datetime.now())


```

#### 2.3 内核Kern核心代码分析

当`udp_recvmsg`执行时，触发`kprobe`转而执行挂载函数`trace_udp_recvmsg`

```python
#include <net/inet_sock.h>

#define MAX_PKT 512
struct dns_data_t {
    u8  pkt[MAX_PKT];
};

BPF_PERF_OUTPUT(dns_events);

// store msghdr pointer captured on syscall entry to parse on syscall return
BPF_HASH(tbl_udp_msg_hdr, u64, struct msghdr *);

// single element per-cpu array to hold the current event off the stack
BPF_PERCPU_ARRAY(dns_data,struct dns_data_t,1);

int trace_udp_recvmsg(struct pt_regs *ctx)
{
    __u64 pid_tgid = bpf_get_current_pid_tgid();
    struct sock *sk = (struct sock *)PT_REGS_PARM1(ctx);
    struct inet_sock *is = inet_sk(sk);

    // only grab port 53 packets, 13568 is ntohs(53)
    if (is->inet_dport == 13568) {
        struct msghdr *msghdr = (struct msghdr *)PT_REGS_PARM2(ctx);
        tbl_udp_msg_hdr.update(&pid_tgid, &msghdr);
    }
    return 0;
}
```

该函数主要作用为53端口过滤,将参数二放入map中

```python
 if (is->inet_dport == 13568) {
        struct msghdr *msghdr = (struct msghdr *)PT_REGS_PARM2(ctx);
        tbl_udp_msg_hdr.update(&pid_tgid, &msghdr);
}
```

`udp_recvmsg`结束时调用`trace_udp_ret_recvmsg`函数。该函数对`msghdr`参数做判断并将数据放入BPF Map中

```python
int trace_udp_ret_recvmsg(struct pt_regs *ctx)
{
    __u64 pid_tgid = bpf_get_current_pid_tgid();
    u32 zero = 0;
    struct msghdr **msgpp = tbl_udp_msg_hdr.lookup(&pid_tgid);
    if (msgpp == 0)
        return 0;

    struct msghdr *msghdr = (struct msghdr *)*msgpp;
    if (msghdr->msg_iter.type != ITER_IOVEC)
        goto delete_and_return;

    int copied = (int)PT_REGS_RC(ctx);
    if (copied < 0)
        goto delete_and_return;
    size_t buflen = (size_t)copied;

    if (buflen > msghdr->msg_iter.iov->iov_len)
        goto delete_and_return;

    if (buflen > MAX_PKT)
        buflen = MAX_PKT;

    struct dns_data_t *data = dns_data.lookup(&zero);
    if (!data) // this should never happen, just making the verifier happy
        return 0;

    void *iovbase = msghdr->msg_iter.iov->iov_base;
    bpf_probe_read(data->pkt, buflen, iovbase);
    dns_events.perf_submit(ctx, data, buflen);

delete_and_return:
    tbl_udp_msg_hdr.delete(&pid_tgid);
    return 0;
}
```

#### 2.4 实验结果

![dns_respon](https://raw.githubusercontent.com/JiaoTuan/Images-JiaoTuan/master/kernel_net/dns_respon.2he7ot4zgze0.png)

<center>图12 DNS响应运行结果</center>

### 3. DNS延迟

#### 3.1 域名解析函数挂载分析

c库提供了域名解析函数如下所示：

```c
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>

int getaddrinfo(const char *node, const char *service,const struct addrinfo *hints, struct addrinfo **res);

struct hostent *gethostbyname(const char *name)

/* GNU extensions */
struct hostent *gethostbyname2(const char *name, int af);
```

#### 3.2 用户态python代码分析

使用uprobe与uretprobe挂载这三个函数。

```python
b = BPF(text=bpf_text)
b.attach_uprobe(name="c", sym="getaddrinfo", fn_name="do_entry")
b.attach_uprobe(name="c", sym="gethostbyname", fn_name="do_entry")
b.attach_uprobe(name="c", sym="gethostbyname2", fn_name="do_entry")
b.attach_uretprobe(name="c", sym="getaddrinfo", fn_name="do_return")
b.attach_uretprobe(name="c", sym="gethostbyname", fn_name="do_return")
b.attach_uretprobe(name="c", sym="gethostbyname2", fn_name="do_return")
```

从Map中取出数据后进行输出：

```python
print("%-9s %-7s %-16s %10s %s" % ("TIME", "PID", "COMM", "LATms", "HOST"))

def print_event(cpu, data, size):
    event = b["events"].event(data)
    print("%-9s %-7d %-16s %10.2f %s" % (strftime("%H:%M:%S"), event.pid,
        event.comm.decode('utf-8', 'replace'), (float(event.delta) / 1000000),
        event.host.decode('utf-8', 'replace')))

b["events"].open_perf_buffer(print_event)
while 1:
    try:
        b.perf_buffer_poll()
    except KeyboardInterrupt:
        exit()
```

#### 3.3 内核kern核心代码分析

使用`uprobe`在函数刚进入时进行事件记录，并使用`kretprobe`在函数退出时进行事件记录，并与进入时时间相减得到DNS延时。

```python
int do_entry(struct pt_regs *ctx) {
  ......
  val.ts = bpf_ktime_get_ns();
}

int do_return(struct pt_regs *ctx) {
  ......
  u64 tsp = bpf_ktime_get_ns();
  ......
  data.delta = tsp - valp->ts;
}
```

使用`bpf_get_current_pid_tgid`获取`pid`:

```c
u64 pid_tgid = bpf_get_current_pid_tgid();
u32 pid = pid_tgid >> 32;
u32 tid = (u32)pid_tgid;
```

使用`bpf_get_current_comm`获取当前进程上下文：

```python
bpf_get_current_comm(&val.comm, sizeof(val.comm)
```

至此将所有获取数据存入BPF Map中。

#### 3.4 实验结果

![dns_lenty](https://raw.githubusercontent.com/JiaoTuan/Images-JiaoTuan/master/kernel_net/dns_lenty.3bafazyh5ii0.png)

<center>图13 DNS延时运行结果</center>


# 网络态势预测

## 1. 重要步骤

### 1.1 训练集与测试集分类

```python
def train_val_split(dataX, dataY, train_percent, output_dim):
    train_size = int(len(dataX) * train_percent)
    x_train, x_val = dataX[: train_size], dataX[train_size:]
    y_train, y_val = dataY[: train_size], dataY[train_size:]
    x_train = Variable(torch.from_numpy(x_train)).type(torch.FloatTensor)
    a, b, c = x_train.size()
    y_train = Variable(torch.from_numpy(y_train.reshape(a, b, output_dim))).type(torch.FloatTensor)
    x_val = Variable(torch.from_numpy(x_val)).type(torch.FloatTensor)
    a, b, c = x_val.size()
    y_val = Variable(torch.from_numpy(y_val.reshape(a, b, output_dim))).type(torch.FloatTensor)
    return x_train, y_train, x_val, y_val
```

### 1.2 定义模型

```python
class LSTM(nn.Module):
    def __init__(self, input_dim, hidden_dim, num_layers, output_dim):
        super(LSTM, self).__init__()
        self.hidden_dim = hidden_dim
        self.num_layers = num_layers
        self.lstm = nn.LSTM(input_dim, hidden_dim, num_layers, batch_first=True, bidirectional=True)
        self.fc_1 = nn.Linear(hidden_dim * 2, hidden_dim)
        self.fc_2 = nn.Linear(hidden_dim, output_dim)

    def forward(self, x):
        y, _ = self.lstm(x)
        y = self.fc_1(y)
        y = self.fc_2(y)
        return y
```

### 1.3 训练模型

```python
def train_model(model, lr, train_loader, val_loader, batch_size):
    loss_func = nn.MSELoss()
    optimizer = torch.optim.Adam(model.parameters(), lr)
    min_val_loss = float('inf')
    bad_count = 0
    for epoch in range(epochs):
        train_loss = 0
        val_loss = 0
        model.train()
        for i, (x, y) in enumerate(train_loader):
            optimizer.zero_grad()
            y_pred = model(x)
            loss = loss_func(y_pred, y)
            loss.backward()
            optimizer.step()
            train_loss += loss.item()
        if epoch % 100 == 0 and epoch != 0:
            model.eval()
            with torch.no_grad():
                for j, (x, y) in enumerate(val_loader):
                    val_pred = model(x)
                    loss = loss_func(val_pred, y)
                    val_loss += loss.item()
                if val_loss < min_val_loss:
                    save_model(model)
                    min_val_loss = val_loss
                    #bad_count = 0
                else:
                    bad_count += 1
            print('Epoch: {}, Train Loss: {}, Val Loss: {}'.format(epoch,
                    train_loss/(len(train_loader) * batch_size), val_loss/(len(val_loader) * batch_size)))
        if bad_count >= 4:
            print('Early Stopping...')
            break
    return model
```

### 1.4 加载训练好模型

```python
def load_model():
    model = None
    if os.path.exists(output_model_path):
        model = torch.load(output_model_path)
    return model
```

## **2. 预测结果**

通过对网络流量的监控预测，可以为网络分析提供一个直观的参考，预测结果如图14-17所示。

**DNS_request:** 

![DNS_request](https://raw.githubusercontent.com/JiaoTuan/Images-JiaoTuan/master/kernel_net/DNS_request.57608829un40.png)

<center>图14 DNS请求预测结果</center>

**DNS_respon:**

![DNS_respon](https://raw.githubusercontent.com/JiaoTuan/Images-JiaoTuan/master/kernel_net/DNS_respon.7d06yp6xu6o0.png)

<center>图15 DNS响应预测结果</center>

**http_request:**

![http_request](https://raw.githubusercontent.com/JiaoTuan/Images-JiaoTuan/master/kernel_net/http_request.46xjzk9mxjc0.png)

<center>图16 HTTP请求预测结果</center>

**http_respon:**

![http_respon](https://raw.githubusercontent.com/JiaoTuan/Images-JiaoTuan/master/kernel_net/http_respon.73bq97nheao0.png)

<center>图17 HTTP响应预测结果</center>