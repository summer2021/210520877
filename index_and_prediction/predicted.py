from torch.utils.data import DataLoader, Dataset
import warnings
import matplotlib.pyplot as plt
import torch
import numpy as np
import pandas as pd
import torch.nn as nn
from torch.autograd import Variable
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error
import os
import sys
file_path = os.path.abspath(__file__)
parent_path = os.path.dirname(os.path.dirname(file_path))
sys.path.append(parent_path)
warnings.filterwarnings('ignore')


np.random.seed(0)
torch.manual_seed(0)
input_data_paths = ['../data/DNS_request.xlsx', '../data/DNS_respon.xlsx', '../data/http_respon.xlsx', '../data/http_send.xlsx']
out_model_path = '../data/OUT_{}.model'
out_test_figure_path = '../data/OUT_{}.png'
out_test_excel_path = '../data/OUT_{}.xlsx'

def read_data(input_data_path):
    df = pd.read_excel(input_data_path, header=None).iloc[:, -1]
    df = df.dropna(axis=0)
    return df.values

def get_dataset(dataset, look_back):
    data_x = []
    data_y = []
    for i in range(len(dataset) - look_back):
        data_x.append([[item] for item in dataset[i: i + look_back]])
        data_y.append([[item] for item in dataset[i + 1: i + look_back + 1]])
    return np.asarray(data_x), np.asarray(data_y)


def train_val_split(dataX, dataY, train_percent, output_dim):
    train_size = int(len(dataX) * train_percent)
    x_train, x_val = dataX[: train_size], dataX[train_size:]
    y_train, y_val = dataY[: train_size], dataY[train_size:]
    x_train = Variable(torch.from_numpy(x_train)).type(torch.FloatTensor)
    a, b, c = x_train.size()
    y_train = Variable(torch.from_numpy(y_train.reshape(a, b, output_dim))).type(torch.FloatTensor)

    x_val = Variable(torch.from_numpy(x_val)).type(torch.FloatTensor)
    a, b, c = x_val.size()
    y_val = Variable(torch.from_numpy(y_val.reshape(a, b, output_dim))).type(torch.FloatTensor)

    return x_train, y_train, x_val, y_val

class Dataset(Dataset):
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __len__(self):
        return len(self.x)

    def __getitem__(self, index):
        x = self.x[index]
        y = self.y[index]
        return x, y

class LSTM(nn.Module):
    def __init__(self, input_dim, hidden_dim, num_layers, output_dim):
        super(LSTM, self).__init__()
        self.hidden_dim = hidden_dim
        self.num_layers = num_layers
        self.lstm = nn.LSTM(input_dim, hidden_dim, num_layers, batch_first=True, bidirectional=True)
        self.fc_1 = nn.Linear(hidden_dim * 2, hidden_dim)
        self.fc_2 = nn.Linear(hidden_dim, output_dim)

    def forward(self, x):
        y, _ = self.lstm(x)
        y = self.fc_1(y)
        y = self.fc_2(y)
        return y


def train_model(model, lr, train_loader, val_loader, batch_size):
    loss_func = nn.MSELoss()
    optimizer = torch.optim.Adam(model.parameters(), lr)
    min_val_loss = float('inf')
    bad_count = 0
    for epoch in range(epochs):
        train_loss = 0
        val_loss = 0
        model.train()
        for i, (x, y) in enumerate(train_loader):
            optimizer.zero_grad()
            y_pred = model(x)
            loss = loss_func(y_pred, y)
            loss.backward()
            optimizer.step()
            train_loss += loss.item()
        if epoch % 100 == 0 and epoch != 0:
            model.eval()
            with torch.no_grad():
                for j, (x, y) in enumerate(val_loader):
                    val_pred = model(x)
                    loss = loss_func(val_pred, y)
                    val_loss += loss.item()
                if val_loss < min_val_loss:
                    save_model(model)
                    min_val_loss = val_loss
                    #bad_count = 0
                else:
                    bad_count += 1
            print('Epoch: {}, Train Loss: {}, Val Loss: {}'.format(epoch,
                    train_loss/(len(train_loader) * batch_size), val_loss/(len(val_loader) * batch_size)))
        if bad_count >= 4:
            print('Early Stopping...')
            break
    return model


def save_model(model):
    torch.save(model, output_model_path)


def load_model():
    model = None
    if os.path.exists(output_model_path):
        model = torch.load(output_model_path)
    return model


def test_model(model, train_loader, val_loader):
    train_true = []
    train_pred = []
    test_true = []
    test_pred = []
    ys_train_raw = train_loader.dataset.y
    ys_train_pred = model(train_loader.dataset.x)
    ys_val_raw = val_loader.dataset.y
    ys_val_pred = model(val_loader.dataset.x)

    for i in range(ys_train_raw.size()[0]):
        train_true.append(ys_train_raw[i, -1, :].data.numpy()[0])
    for i in range(ys_train_pred.size()[0]):
        train_pred.append(ys_train_pred[i, -1, :].data.numpy()[0])

    for i in range(ys_val_raw.size()[0]):
        test_true.append(ys_val_raw[i, -1, :].data.numpy()[0])
    for i in range(ys_val_pred.size()[0]):
        test_pred.append(ys_val_pred[i, -1, :].data.numpy()[0])

    y_true = train_true + test_true
    y_pred = train_pred + test_pred

    x = [i for i in range(len(y_true))]
    plt.figure(figsize=(15, 7))
    plt.plot(y_true, 'lightgreen', label='real')
    plt.plot(y_pred, 'steelblue', label='predict')
    plt.title('test data')
    plt.legend(loc='best')
    plt.savefig(output_test_figure_path)
    #plt.show()
    return np.array(y_true), np.array(y_pred)


def calculate(origin_y_ture, origin_y_pred):
    MAE = mean_absolute_error(origin_y_ture, origin_y_pred)
    RMSE = mean_squared_error(origin_y_ture, origin_y_pred) ** 0.5
    return MAE, RMSE


def save_to_excel(origin_y_true, origin_y_pred, output_train_excel_path):
    data_list = []
    for i in range(len(origin_y_true)):
        data_dict = {'真实值': origin_y_true[i], '预测值': origin_y_pred[i]}
        data_list.append(data_dict)
    df = pd.DataFrame(data_list)
    writer = pd.ExcelWriter(output_train_excel_path)
    df.to_excel(writer, index=False)
    writer.save()

if __name__ == "__main__":

    for input_data_path in input_data_paths:
        f_name = input_data_path.split('/')[-1].replace('.xlsx', '')
        output_model_path = out_model_path.format(f_name)
        output_test_figure_path = out_test_figure_path.format(f_name)
        output_test_excel_path = out_test_excel_path.format(f_name)

        look_back = 3 
        batch_size = 20
        train_percent = 0.9

        input_dim = 1 
        hidden_dim = 16  
        num_layers = 2 
        output_dim = 1  
        epochs = 8000  
        lr = 5e-5  

        dataset = read_data(input_data_path)
        x_data, y_data = get_dataset(dataset, look_back)
        x_train, y_train, x_val, y_val = train_val_split(x_data, y_data, train_percent, output_dim)
        train_set = Dataset(x_train, y_train)
        val_set = Dataset(x_val, y_val)
        train_loader = DataLoader(train_set, batch_size, shuffle=False)
        val_loader = DataLoader(val_set, batch_size, shuffle=False)
        print('数据准备完成')

        model = load_model()
        if model:
            print('加载模型完成')
        else:
            model = LSTM(input_dim, hidden_dim, num_layers, output_dim)
        model = train_model(model, lr, train_loader, val_loader, batch_size)
        print('训练模型完成')
        y_test_true, y_test_pred = test_model(model, train_loader, val_loader)
        MAE, RMSE = calculate(y_test_true, y_test_pred)
        print('MAE = {}\nRMSE={}'.format(MAE, RMSE))
        save_to_excel(y_test_true, y_test_pred, output_test_excel_path)